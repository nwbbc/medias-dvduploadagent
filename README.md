# README #

An electron app that runs on a local PC to automate uploading DVD Video files to the LAN storage.

### Why is this agent needed? ###

All audio files (mp3) are saved on cloud storage. 
All DVD/video files need to remain on a local network storage. (Doesn’t make sense to put video files on cloud storage now.)
Therefore, the DvdUploadAgent will need to be present to copy the video files from the DVD to the right place on the network, and communicate to the server what the status of those files are.

(Audio files will be handled directly in the web app and NOT the Agent.)

## Flow of the application ##

Needs a basic GUI (electron-based). Does not need to run as service.

- HTTP: 
  - Get list of recent services
  - Add new service (basic)
  - Update service disc status info
- GUI: 
  - Show list of services
  - Allow user to select a service
  - Allow user to add new service with basic Title/Speaker/Date
- IO: 
  - Check if files already exist on network path
  - Copy files from external drive to network path.
  - SCAN all files/folders in network path and report back to server what it finds. (to sync what is actually exists on disk)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact